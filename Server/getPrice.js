import { getData } from "./getData";

export async function getPrice(productName) {
    let price
    let data = await getData()
    data.forEach(data => {
        if (data.productName === productName) {
            price = data.price
        }
    })
    return parseInt(price)
}