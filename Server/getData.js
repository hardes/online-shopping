import path from 'path';
import { promises as fs } from 'fs';

export async function getData() {
    const dataDirectory = path.join(process.cwd(), 'Data');
    let fileContents = await fs.readFile(dataDirectory + '/products.json', 'utf8');
    const json = JSON.parse(fileContents)
    return json;
}