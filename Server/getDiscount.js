import { randomUUID } from "crypto";
import { promises as fs } from 'fs';
const n = 3
let dataStructure = { "Total": { "items": 0, "amount": 0 }, "Discount": { "amount": 0, "currentCode": null, "codes": [] }, "Orders": 0 }

export function applyDiscount(price, code, dataObj) {
    if (dataObj.Discount.currentCode === code) {
        dataObj.Discount.currentCode = null
        let afterDiscount = price - (0.1 * price)
        dataObj.Discount.amount += price - afterDiscount
        dataObj.Orders = 0
        dataObj.Total.amount += afterDiscount
    }
    else {
        if (dataObj.Orders % n === 0) {
            let random = randomUUID()
            dataObj.Discount.codes.push(random)
            dataObj.Discount.currentCode = random
        }
    }
    return dataObj
}

export async function getDiscountCode() {
    let dataObj
    let dataFile = await fs.readFile('Data/userData.json', 'utf8');
    if (dataFile === '') dataObj = dataStructure
    else dataObj = JSON.parse(dataFile)
    if (dataObj.Orders % n === 0) {
        let random = randomUUID()
        dataObj.Discount.codes.push(random)
        dataObj.Discount.currentCode = random
        fs.writeFile('Data/userData.json', JSON.stringify(dataObj));
        return dataObj.Discount.currentCode
    }
    return null
}