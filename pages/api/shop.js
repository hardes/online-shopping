import { getPrice } from '@/Server/getPrice'
import { applyDiscount } from '@/Server/getDiscount'
import { promises as fs } from 'fs';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    let dataFile = await fs.readFile('Data/userData.json', 'utf8');
    let dataObj = JSON.parse(dataFile)
    dataObj.Orders += 1
    let ItemsArr = req.body.ProductData.map((data) => data.count)
    dataObj.Total.items += ItemsArr.reduce((total, num) => total + num)
    let amountArrPromise = req.body.ProductData.map(async (data) => {
      let bill = 0
      let price = await getPrice(data.name)
      for (let i = 0; i < data.count; i++) {
        bill += price
      }
      return bill
    })
    let amountArr = await Promise.all(amountArrPromise)
    let totalBill = amountArr.reduce((total, num) => total + num)
    dataObj = await applyDiscount(totalBill, req.body.CouponCode, dataObj)
    fs.writeFile('Data/userData.json', JSON.stringify(dataObj));
    res.send(dataObj)
  }
  res.end()
}
