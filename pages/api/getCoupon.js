import { getDiscountCode } from "@/Server/getDiscount";
export default async function handler(req, res) {
    if (req.method === 'GET') {
        res.send(await getDiscountCode())
    }
    res.end()
}