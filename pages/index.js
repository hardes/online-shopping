
import styles from '@/styles/Home.module.css'
import { getData } from '@/Server/getData'
import { useContext, useEffect } from 'react'
import { SetHead } from '@/Components/Header'
import { AppContext } from './_app'

export default function Home({ Data }) {
  const { Cart, setCart, setCount } = useContext(AppContext);
  useEffect(() => {
    if (Cart === undefined) {
      setCart(() => {
        return Data.map((d) => { return { name: d.productName, count: 0, price: d.price } })
      })
    }
  }, [])
  function AddToCart(index, add) {
    let value = Cart[index].count
    add ? value++ : value--
    if (value < 0) value = 0
    else add ? setCount(prev => prev + 1) : setCount(prev => prev - 1)
    setCart(prev => {
      prev[index].count = value
      return [...prev]
    })
  }
  if (Cart !== undefined) {
    return (
      <>
        <SetHead />
        <section id={styles.mainSection}>
          {Data.map((d, index) => {
            return (
              <div className={styles.Box} key={d.productName}>
                <h1>{d.productName}</h1>
                <p>Price: &#8377;{d.price}</p>
                <div className={styles.actionBlock}>
                  <button onClick={() => AddToCart(index, true)}>Add</button>
                  <h4>{Cart[index].count}</h4>
                  <button onClick={() => AddToCart(index, false)}>Remove</button>
                </div>
              </div>
            )
          })
          }
        </section>
      </>
    )
  }
}

export async function getServerSideProps() {
  let data = await getData()
  return {
    props: { Data: data },
  }
}
