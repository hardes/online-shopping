import { useEffect, useState, useRef, useContext } from 'react'
import styles from '@/styles/Home.module.css'
import { AppContext } from './_app';

export default function Buy() {
    const { Cart, setCart, count, setCount } = useContext(AppContext);
    const [getCoupon, setGetCoupon] = useState(null)
    const [sendCoupon, setSendCoupon] = useState(null)
    const [doesSendCoupon, setDoesSendCoupon] = useState(false)
    const runOnlyOnce = useRef(true)
    useEffect(() => {
        if (runOnlyOnce.current) {
            fetch('/api/getCoupon')
                .then(response => response.text())
                .then(data => {
                    setGetCoupon(data)
                });
            runOnlyOnce.current = false
        }
    }, [])
    useEffect(() => {
        if (doesSendCoupon === true && getCoupon !== '') {
            setSendCoupon(getCoupon)
            setDoesSendCoupon(false)
            setGetCoupon('')
        }
    }, [doesSendCoupon, getCoupon])
    function SendData() {
        setCart((prev) => {
            return prev.map((d) => { return { name: d.name, count: 0, price: d.price } }
            )
        })
        setCount(0)
        let CartData = { ProductData: Cart.filter(data => data.count !== 0), CouponCode: sendCoupon }
        fetch("/api/shop", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(CartData)
        })
            .then(response => response.text())
            .then(data => {
                setGetCoupon(data)
            });
    }
    return (
        <>
            {count > 0 ? <div id={styles.BuyBlock}>
                <ul>{Cart.map((item) => {
                    if (item.count > 0) {
                        let totalPrice = 0
                        for (let i = 0; i < item.count; i++) {
                            totalPrice += parseInt(item.price)
                        }
                        return <li key={item.name}><b>{item.count} {item.name} rs. {totalPrice}</b></li>
                    }
                })}</ul>
                {getCoupon !== "" ? <button onClick={() => setDoesSendCoupon(true)}>Click to get 10% Discount</button> : ''}
                <button onClick={SendData}>Buy</button>
            </div> : <h1>No Items Selected</h1>}
        </>
    )
}