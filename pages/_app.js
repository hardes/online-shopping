import '@/styles/globals.css'
import Header from '@/Components/Header'
import { createContext, useState } from 'react';

export const AppContext = createContext();
export default function App({ Component, pageProps }) {
  const [Cart, setCart] = useState()
  const [count, setCount] = useState(0)
  return (
    <AppContext.Provider value={{ Cart, setCart, count, setCount }}>
      <Header />
      <Component {...pageProps} />
    </AppContext.Provider>
  )
}


